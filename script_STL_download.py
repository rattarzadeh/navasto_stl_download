import argparse
from mlutils.dbconfig import ws_db_conn
import subprocess
from multiprocessing import Pool


no_of_cases_to_download = 1000

import pandas as pd
df = pd.read_csv('file.csv')
print("Number of CADs: ", len(df))


GeoBase_uuid = list(df.iloc[0:no_of_cases_to_download]["Spec Snapshots → Domain Geometry Base UUID"])
#GeoBase_uuid="84428bd0-185e-41b5-8e26-2aca0b257852"
print(GeoBase_uuid)
print("This is the GeoBase_uuid to make query", GeoBase_uuid[4])

import requests
import urllib.request

for element in range(len(GeoBase_uuid)):
    print("----------------->>>>>> NEW CASE")
    url = 'http://service-long.internal.simscale.com/internal-csm/operations/%s/query'%GeoBase_uuid[element]
    myobj = {
        "query": "tessellate",
        "parameters": {
            "Unit": "m",
            "CalculateArea": True,
            "TolerateFaults": True,
            "CheckNonManifold": False,
            "TessellationType": "maxFacetWidth"
        }
    }
    x = requests.post(url, json = myobj)
    # print(x.text)

    import ast
    dictionary = ast.literal_eval(x.text)
    # if dictionary["objectId"]:
    #     objectId = dictionary["objectId"]
    #     print("ObjectID:",objectId,"GeoBaseUUID:", GeoBase_uuid[element])
    #     command2 = f'cd CADs && curl "http://dm.internal.simscale.com/dm/{objectId}" -o {objectId}.zip'# && unzip {objectId}.zip && rm {objectId}.zip'
    #     print(command2)
    #     subprocess.getoutput(command2)
    Good_simulations = []
    try:
        print("no of bodies:", dictionary["numBodies"])
        if dictionary["numBodies"]==1:
            objectId = dictionary["objectId"]
            print("GeoBaseUUID:", GeoBase_uuid[element], "ObjectID:",objectId)
            command2 = f'cd CADs && curl "http://dm.internal.simscale.com/dm/{objectId}" -o {objectId}.zip'# && unzip {objectId}.zip && rm {objectId}.zip'
            print(command2)
            subprocess.getoutput(command2)
            Good_simulations.append(GeoBase_uuid[element])
            print(Good_simulations)

    except KeyError:
        print("failed!!!!", "GeoBaseUUID:", GeoBase_uuid[element])
        pass


        

